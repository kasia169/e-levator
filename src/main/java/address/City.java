package address;

public enum City {

    LUBLIN("Lublin"), WARSZAWA("Warszawa"), GDAŃSK("Gdańsk"), ŁUDŹ("Łudź"),
    STALOWA_WOLA("Stalowa Wola"), KATOWICE("Katowice");

    private String cityName;

    City(String cityName) {
        this.cityName = cityName;
    }

    public String getCityName() {
        return cityName;
    }

    @Override
    public String toString() {
        return getCityName();
    }
}
