package address;

final public class Address {
    private City city;
    private String streetName;
    private int blockNo;
    private int appartmentNo;
    private String postCode;

    public Address(City city, String streetName) {
        this.city = city;
        this.streetName = streetName;
    }

    public City getCity() {
        return city;
    }

    public Address setCity(City city) {
        this.city = city;
        return this;
    }

    public String getStreetName() {
        return streetName;
    }

    public Address setStreetName(String streetName) {
        this.streetName = streetName;
        return this;
    }

    public int getBlockNo() {
        return blockNo;
    }

    public Address setBlockNo(int blockNo) {
        this.blockNo = blockNo;
        return this;
    }

    public int getAppartmentNo() {
        return appartmentNo;
    }

    public Address setAppartmentNo(int appartmentNo) {
        this.appartmentNo = appartmentNo;
        return this;
    }

    public String getPostCode() {
        return postCode;
    }

    public Address setPostCode(String postCode) {
        this.postCode = postCode;
        return this;
    }

    @Override
    public String toString() {
        return getCity() + " na ul. " + getStreetName() + " " + getBlockNo();
    }
}
