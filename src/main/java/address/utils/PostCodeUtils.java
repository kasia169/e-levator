package address.utils;

import com.sun.istack.internal.Nullable;

import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PostCodeUtils {

    private PostCodeUtils() {

    }

    public static boolean isCorrect(String code, @Nullable String region) {
        boolean result = false;

        if (region == null) {
            Locale locale = Locale.getDefault();
            region = locale.getISO3Country();
        }

        Pattern pattern = null;

        switch (region) {
            case "POL":
                pattern = Pattern.compile("\\d{2}-\\d{3}");
                break;

            case "USA":
                pattern=Pattern.compile("\\d{5}");
                break;

        }

        if (pattern != null) {
            Matcher matcher = pattern.matcher(code);
            result = matcher.matches();
        }


        return result;
    }
}
