import address.Address;
import address.City;
import address.utils.PostCodeUtils;
import block.BlockWithElevators;
import elevator.Elevator;
import elevator.PassengerElevator;
import floor.Floor;
import person.ElevatorUser;
import person.Person;
import weight.WeightMetrics;
import weight.Weigth;

import javax.jws.soap.SOAPBinding;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Person person = new Person("Bartek");
        person.setName("Dec");
        person.setWeight(new Weigth(70, WeightMetrics.KG));
        Address adress = new Address(City.LUBLIN,"Wesoła").setCity(City.LUBLIN).setStreetName("Wesoła").setBlockNo(1)
                .setAppartmentNo(2).setPostCode("21-132");
        person.setAddress(adress);

        System.out.println(person);

        PostCodeUtils.isCorrect("21-132", "POL");
///////////////////////////////////////////////////////////////////////
        //List<Elevator> elevator = new ArrayList<>();

        //List<ElevatorUser> user = new ArrayList<>();

        //HashMap<Floor, List<ElevatorUser>> mapa = new HashMap<>();

//Lista pięter
        List<Floor> floors = new ArrayList<>(Arrays.asList(new Floor(-1),
                new Floor(0), new Floor(1), new Floor(2), new Floor(3),
                new Floor(4), new Floor(5), new Floor(6), new Floor(7), new Floor(8),
                new Floor(9), new Floor(10)));

// tworzymy blok z winą
        BlockWithElevators blockWithElevators = new BlockWithElevators(floors);
//tworzymy windę
        Elevator elevator = new PassengerElevator();
        // dodajemy dostępne piętra
        elevator.setAvailableFloors(blockWithElevators.getFloors());
        //ustawiamy winde na piętrze 0
        elevator.setCurrentFloor(new Floor(0));
        //definijemy dopuszczalne obciązenie windy
        elevator.setMaxLoad(new Weigth(800, WeightMetrics.KG));
        // dodaje winde do bloku
        blockWithElevators.setElevators(Arrays.asList(elevator));

//        ElevatorUser user1 = new ElevatorUser("bartek");
//        ElevatorUser user2 = new ElevatorUser("robert");
//
//        blockWithElevators.addElevatorUserToQueue(new Floor(1),user1);
//        blockWithElevators.addElevatorUserToQueue(new Floor(1),user2);
//
//        Floor floor = new Floor(2);
//        blockWithElevators.addElevatorUserToQueue(floor,user1);
//        blockWithElevators.addElevatorUserToQueue(floor,user2);

        Scanner sc = new Scanner(System.in);
        String exit;
        do{
            System.out.println("Podaj imię");
            String name = sc.next();
            ElevatorUser userx = new ElevatorUser(name);
            System.out.println("Podaj piętro");
            int pietro = sc.nextInt();
            Floor florex = new Floor(pietro);
            blockWithElevators.addElevatorUserToQueue(florex, userx);
            System.out.print("Jeśli chcesz wyjśc wciśnij exit ");
            exit = sc.next();
        } while (!exit.equals("exit"));
    }




}
