package block;

import address.Address;
import floor.Floor;

import java.util.List;

public abstract class Block {
    private List<Floor> floors;
    private Address address;

    public Block(List<Floor> floors) {
        this.floors = floors;
    }

    public List<Floor> getFloors() {
        return floors;
    }

    public void setFloors(List<Floor> floors) {
        this.floors = floors;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
    public abstract boolean hasElevator();
}
