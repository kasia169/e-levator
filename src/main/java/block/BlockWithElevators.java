package block;

import elevator.Elevator;
import floor.Floor;
import person.ElevatorUser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class BlockWithElevators extends Block implements QueueChecker {
    private List<Elevator> elevators;
    private HashMap<Floor, List<ElevatorUser>> elevatorQueue;

    public BlockWithElevators(List<Floor> floors) {
        super(floors);
        elevatorQueue = new HashMap<>();
    }

    public List<Elevator> getElevators() {
        return elevators;
    }

    public void setElevators(List<Elevator> elevators) {
        this.elevators = elevators;
    }

    public HashMap<Floor, List<ElevatorUser>> getElevatorQueue() {
        return elevatorQueue;
    }

    public void setElevatorQueue(HashMap<Floor, List<ElevatorUser>> elevatorQueue) {
        this.elevatorQueue = elevatorQueue;
    }

    //@Override
    public boolean hasElevator() {
        return true;
    }

    //dodajemy osobę do kolejki

    public void addElevatorUserToQueue(Floor floor, ElevatorUser elevatorUser) {

        List<ElevatorUser> listOfQueueOnFloor = getElevatorQueue().get(floor);

        if (listOfQueueOnFloor != null) {
            listOfQueueOnFloor.add(elevatorUser);
        } else {
            List<ElevatorUser> newList = new ArrayList<>();
            newList.add(elevatorUser);
            elevatorQueue.put(floor, newList);
        }
        System.out.println("Użytkownik windy o imieniu:  " + elevatorUser.getFirstName() + " chce jechać na piętro: "
                + floor.getFloorNo());
    }

    @Override
    public boolean isAnyQueueOnFloor(Floor floor) {
        return elevatorQueue.containsKey(floor);
    }

    @Override
    public List<ElevatorUser> getPersonFromFloor(Floor floor) {
            return elevatorQueue.get(floor);
        }
    }
