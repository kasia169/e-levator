package block;

import floor.Floor;
import person.ElevatorUser;

import java.util.List;

/**
 * Created by RENT on 2017-08-25.
 */
public interface QueueChecker {

    boolean isAnyQueueOnFloor (Floor floor);
    List<ElevatorUser> getPersonFromFloor(Floor floor);

}
