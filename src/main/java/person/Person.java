package person;

import address.Address;
import weight.Weigth;

public class Person {
    private String FirstName;
    private String Name;
    private Address address;
    private Weigth weight;

    public Person(String firstName) {
        FirstName = firstName;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Weigth getWeight() {
        return weight;
    }

    public void setWeight(Weigth weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "Nazywam się " + getFirstName() + " " + getName() + ". Mieszkam w " + getAddress();
    }
}
