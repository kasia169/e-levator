package person;

import floor.Floor;

public class ElevatorUser extends Person {
    public static final int AVERAGE_WEIGHT_OF_PASSENGER = 90;
    boolean isInElevator;
    Floor wantToGoFloor;
    Floor currentFloor;

    public ElevatorUser(String firstName) {
        super(firstName);
    }

    public boolean isInElevator() {
        return isInElevator;
    }

    public ElevatorUser setInElevator(boolean inElevator) {
        isInElevator = inElevator;
        return this;
    }

    public Floor getWantToGoFloor() {
        return wantToGoFloor;
    }

    public ElevatorUser setWantToGoFloor(Floor wantToGoFloor) {
        this.wantToGoFloor = wantToGoFloor;
        return this;
    }

    public Floor getCurrentFloor() {
        return currentFloor;
    }

    public ElevatorUser setCurrentFloor(Floor currentFloor) {
        this.currentFloor = currentFloor;
        return this;
    }


}
