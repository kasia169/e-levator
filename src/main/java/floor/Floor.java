package floor;

public class Floor {
    private Integer floorNo;

    public Floor(Integer floorNo) {
        this.floorNo = floorNo;
    }

    public Integer getFloorNo() {
        return floorNo;
    }

    public void setFloorNo(Integer floorNo) {
        this.floorNo = floorNo;
    }

    @Override
    public boolean equals(Object obj) {
        boolean result = false;
        if (obj instanceof Floor) {
            Floor temp = (Floor) obj;
            if (floorNo == temp.floorNo) {
                result = true;
            }
        }
        return result;
    }

    @Override
    public int hashCode() {
        return floorNo;
    }

}
