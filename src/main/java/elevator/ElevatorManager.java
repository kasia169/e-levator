package elevator;

import block.BlockWithElevators;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class ElevatorManager {

    private BlockWithElevators blockWithElevators;

    AtomicBoolean isWorking = new AtomicBoolean(false);

    List<Elevator> winda = new ArrayList<>();

    public ElevatorManager(BlockWithElevators blockWithElevators) {
        this.blockWithElevators = blockWithElevators;
    }

    //    public void isStartAleready(){
//        boolean result = true;
//        Direction direction = blockWithElevators.getElevators().get(0).getDirection();
//        if(direction == Direction.NONE){
//            result = false;
//        }
//
//        return;
//    }

//    public void start(){
//        Elevator elevator = blockWithElevators.getElevators().get(0);
//        boolean isStart = true;


    public boolean startWork(){
        if(isWorking.get()){
            return false;
        }
        isWorking.set(true);

       new Thread(() -> {
           while(isWorking.get()){
           moveElevators();
           printState();
           try{
               Thread.sleep(2000);
           } catch (InterruptedException e){
               e.printStackTrace();
           }
       }}).start();

        return true;
    }

    public void printState(){
        List<Elevator> winda = blockWithElevators.getElevators();

        winda.stream().forEach(windy -> System.out.println(windy.toString()));
    }


 //   public void nextStep(){
    //    List<Elevator> passangerList = blockWithElevators.getElevators().get(0).getRequestFloors()
   //     for(Elevator winda : blockWithElevators.getElevators()){




    public void moveElevators(){

        List<Elevator> winda = blockWithElevators.getElevators();
        winda.stream().forEach(e -> e.nextStep());

    }
}
