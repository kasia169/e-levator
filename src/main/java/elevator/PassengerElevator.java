package elevator;

import floor.Floor;
import person.ElevatorUser;
import weight.WeightMetrics;
import weight.Weigth;

import java.util.ArrayList;
import java.util.List;

public class PassengerElevator extends Elevator {

    List<ElevatorUser> personsOnBoard = new ArrayList<>();
    private short maxLoadPerson;

    public List<ElevatorUser> getPersonsOnBoard() {
        return personsOnBoard;
    }

    public void setPersonsOnBoard(List<ElevatorUser> personsOnBoard) {
        this.personsOnBoard = personsOnBoard;
    }

    public short getMaxLoadPerson() {
        return maxLoadPerson;
    }

    public void setMaxLoadPerson(short maxLoadPerson) {
        this.maxLoadPerson = maxLoadPerson;
    }

    @Override
    public void setMaxLoad(Weigth maxLoad) {
        super.setMaxLoad(maxLoad);

        float safeLoad = 0.9f * maxLoad.getAmount();

        if (maxLoad.getWeightMetrics() == WeightMetrics.KG) {
            short numOfPersons = (short) (safeLoad / ElevatorUser.AVERAGE_WEIGHT_OF_PASSENGER);
            setMaxLoadPerson(numOfPersons);
        } else {
            short numOfPersons = (short) ((safeLoad * 2.2) / ElevatorUser.AVERAGE_WEIGHT_OF_PASSENGER);
            setMaxLoadPerson(numOfPersons);
        }
    }
    @Override
    public  void nextStep(){
        for (ElevatorUser user : personsOnBoard){
            if(user.getWantToGoFloor().equals((getCurrentFloor()))){
                personsOnBoard.remove(user);
                System.out.println(user + "użytkownik, którzy chcieli wyjść wyszli");
            }
            return;
        }

    }
}
