package elevator;

import block.QueueChecker;
import floor.Floor;
import weight.Weigth;

import java.util.ArrayList;
import java.util.List;

public abstract class Elevator {
    private List<Floor> availableFloors;
    private List<Floor> requestFloors = new ArrayList<>();
    private Floor currentFloor;
    private boolean serviceBreak = false;
    protected Weigth maxLoad;
    Direction direction;
    private QueueChecker queueChecker;

    public Elevator(QueueChecker queueChecker) {
        this.queueChecker = queueChecker;
    }

    public QueueChecker getQueueChecker() {
        return queueChecker;
    }

    public void setQueueChecker(QueueChecker queueChecker) {
        this.queueChecker = queueChecker;
    }

    public List<Floor> getAvailableFloors() {
        return availableFloors;
    }

    public List<Floor> getRequestFloors() {
        return requestFloors;
    }

    public void setRequestFloors(List<Floor> requestFloors) {
        this.requestFloors = requestFloors;
    }

    public Floor getCurrentFloor() {
        return currentFloor;
    }

    public void setCurrentFloor(Floor currentFloor) {
        this.currentFloor = currentFloor;
    }

    public boolean isServiceBreak() {
        return serviceBreak;
    }

    public void setServiceBreak(boolean serviceBreak) {
        this.serviceBreak = serviceBreak;
    }

    public Weigth getMaxLoad() {
        return maxLoad;
    }

    public void setMaxLoad(Weigth maxLoad) {
        this.maxLoad = new Weigth(maxLoad.getWeight(maxLoad.getWeightMetrics()) - 100, maxLoad.getWeightMetrics());
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public void setAvailableFloors(List<Floor> availableFloors) {
        this.availableFloors = availableFloors;
    }

    @Override
    public String toString(){
        return  String.format("ID windy to%s jest na piętrze:",  super.toString(), currentFloor.getFloorNo());
    }

    public abstract void nextStep();
}
