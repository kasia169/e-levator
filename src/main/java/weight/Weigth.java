package weight;
//dopisać testy jednostkowe
public class Weigth {
    float amount;
    WeightMetrics weightMetrics;

    public Weigth(float amount, WeightMetrics weightMetrics) {
        this.amount = amount;
        this.weightMetrics = weightMetrics;
    }



    public float getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public WeightMetrics getWeightMetrics() {
        return weightMetrics;
    }

    public void setWeightMetrics(WeightMetrics weightMetrics) {
        this.weightMetrics = weightMetrics;
    }

    public float getWeight(WeightMetrics weightMetrics) {
        if (weightMetrics == this.getWeightMetrics()) {
            return this.getAmount();
        } else if (this.getWeightMetrics() == WeightMetrics.LB) {
            return this.getAmount() / 0.45f;
        } else {
            return this.getAmount() * 0.45f;
        }
    }

    @Override
    public String toString() {
        return getAmount() + " " + getWeightMetrics();
    }
}
